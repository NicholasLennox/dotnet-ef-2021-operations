﻿using EFCF_Athletes.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace EFCF_Athletes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            CoachDbContext context = new CoachDbContext();

            // Query out some athlete
            var result = context.Certification.Include(c => c.Coaches).ThenInclude(c => c.Athletes).Where(c => c.Id == 1);
            Console.WriteLine(result.ToQueryString());

            Coach test = new()
            {
                Name = "Dewald",
                Gender = "Manly man",
                DOB = DateTime.MinValue,
                Awards = 1
            };
            Athlete tA = new Athlete
            {
                Name = "Nicholas",
                Gender = "Simp",
                DOB = DateTime.Now.AddDays(-1),
                Records = 0,
                Coach = test
            };
            context.Athlete.Add(tA);
            context.SaveChanges();
        }
    }
}
