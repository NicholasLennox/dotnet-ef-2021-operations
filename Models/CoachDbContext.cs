﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCF_Athletes.Models
{
    public class CoachDbContext : DbContext
    {
        // Tables
        public DbSet<Athlete> Athlete { get; set; }
        public DbSet<Coach> Coach { get; set; }
        public DbSet<Certification> Certification { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source = MININT-07CHMFP\\SQLEXPRESS ; Initial Catalog = Coaches2021; Integrated Security = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Certification>().HasData(new Certification { Id = 1, Name = "Boxing" });
            modelBuilder.Entity<Certification>().HasData(new Certification { Id = 2, Name = "Running" });
            modelBuilder.Entity<Certification>().HasData(new Certification { Id = 3, Name = "Badass" });

            modelBuilder.Entity<Coach>().HasData(new Coach() { Id = 1, Name = "John McIntyre", DOB = DateTime.Now.AddYears(-40), Gender = "Male", Awards = 10 });
            modelBuilder.Entity<Coach>().HasData(new Coach() { Id = 2, Name = "Renate Blindheim", DOB = DateTime.Now.AddYears(-30), Gender = "Female", Awards = 15 });
            modelBuilder.Entity<Coach>().HasData(new Coach() { Id = 3, Name = "Phil Jackson", DOB = DateTime.Now.AddYears(-74), Gender = "Male", Awards = 52 });
            modelBuilder.Entity<Coach>().HasData(new Coach() { Id = 4, Name = "Christine Girard", DOB = DateTime.Now.AddYears(-35), Gender = "Female", Awards = 38 });

            modelBuilder.Entity<Athlete>().HasData(new Athlete() { Id = 1, Name = "Micheal Jordan", CoachId = 3, Gender = "Male", DOB = new DateTime(1963, 2, 17), Records = 10 });
            modelBuilder.Entity<Athlete>().HasData(new Athlete() { Id = 2, Name = "Thomas Ulsrud", CoachId = 4, Gender = "Male", DOB = new DateTime(1971, 10, 21), Records = 15 });
            modelBuilder.Entity<Athlete>().HasData(new Athlete() { Id = 3, Name = "Kjetil Andre Aamodt", CoachId = 2, Gender = "Male", DOB = new DateTime(1971, 9, 2), Records = 28 });
            modelBuilder.Entity<Athlete>().HasData(new Athlete() { Id = 4, Name = "David Beckham", CoachId = 1, Gender = "Male", DOB = new DateTime(1975, 5, 2), Records = 52 });
            modelBuilder.Entity<Athlete>().HasData(new Athlete() { Id = 5, Name = "Usain Bolt", CoachId = 1, Gender = "Male", DOB = new DateTime(1986, 08, 21), Records = 42 });

        }
    }
}
