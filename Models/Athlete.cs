﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCF_Athletes.Models
{
    public class Athlete
    {
        // Pk
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        public int Records { get; set; }

        // Everything relationship related
        public int CoachId { get; set; }
        public Coach Coach { get; set; }


    }
}
