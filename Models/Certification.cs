﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCF_Athletes.Models
{
    public class Certification
    {
        // PK
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        // Relationships
        public ICollection<Coach> Coaches { get; set; }

    }
}
