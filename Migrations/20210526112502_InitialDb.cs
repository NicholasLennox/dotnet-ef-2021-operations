﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCF_Athletes.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Certification",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coach",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    DOB = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Awards = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coach", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Athlete",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    DOB = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Records = table.Column<int>(type: "int", nullable: false),
                    CoachId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Athlete", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Athlete_Coach_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coach",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CertificationCoach",
                columns: table => new
                {
                    CertificationsId = table.Column<int>(type: "int", nullable: false),
                    CoachesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CertificationCoach", x => new { x.CertificationsId, x.CoachesId });
                    table.ForeignKey(
                        name: "FK_CertificationCoach_Certification_CertificationsId",
                        column: x => x.CertificationsId,
                        principalTable: "Certification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CertificationCoach_Coach_CoachesId",
                        column: x => x.CoachesId,
                        principalTable: "Coach",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Certification",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Boxing" },
                    { 2, "Running" },
                    { 3, "Badass" }
                });

            migrationBuilder.InsertData(
                table: "Coach",
                columns: new[] { "Id", "Awards", "DOB", "Gender", "Name" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(1981, 5, 26, 13, 25, 1, 313, DateTimeKind.Local).AddTicks(2490), "Male", "John McIntyre" },
                    { 2, 15, new DateTime(1991, 5, 26, 13, 25, 1, 315, DateTimeKind.Local).AddTicks(9530), "Female", "Renate Blindheim" },
                    { 3, 52, new DateTime(1947, 5, 26, 13, 25, 1, 315, DateTimeKind.Local).AddTicks(9586), "Male", "Phil Jackson" },
                    { 4, 38, new DateTime(1986, 5, 26, 13, 25, 1, 315, DateTimeKind.Local).AddTicks(9607), "Female", "Christine Girard" }
                });

            migrationBuilder.InsertData(
                table: "Athlete",
                columns: new[] { "Id", "CoachId", "DOB", "Gender", "Name", "Records" },
                values: new object[,]
                {
                    { 4, 1, new DateTime(1975, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "David Beckham", 52 },
                    { 5, 1, new DateTime(1986, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Usain Bolt", 42 },
                    { 3, 2, new DateTime(1971, 9, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Kjetil Andre Aamodt", 28 },
                    { 1, 3, new DateTime(1963, 2, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Micheal Jordan", 10 },
                    { 2, 4, new DateTime(1971, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Thomas Ulsrud", 15 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Athlete_CoachId",
                table: "Athlete",
                column: "CoachId");

            migrationBuilder.CreateIndex(
                name: "IX_CertificationCoach_CoachesId",
                table: "CertificationCoach",
                column: "CoachesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Athlete");

            migrationBuilder.DropTable(
                name: "CertificationCoach");

            migrationBuilder.DropTable(
                name: "Certification");

            migrationBuilder.DropTable(
                name: "Coach");
        }
    }
}
